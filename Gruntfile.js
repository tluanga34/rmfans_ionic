
module.exports = function(grunt) {
	
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	grunt.initConfig({
		concat: {
	    	js: {
	     		src: ['lib/jquery/dist/jquery.min.js','lib/velocity/velocity.js','www/lib/angular/angular.min.js', 'www/lib/angular-route/angular-route.js', 'www/lib/scheduler/codebase/dhtmlxscheduler.js','www/site_config.js','www/js/app.js','www/js/services.js','www/js/controllers.js','www/js/directives.js','www/js/filters.js'],
	    		dest: 'www/dist/main.js',
	    	},
	    	css : {
	    		src : ['www/lib/bootstrap/dist/css/bootstrap.min.css','www/css/style.css'],
	    		dest : 'www/dist/main.css'
	    	}
	 	},
	 	uglify: {
			targets: {
					options : {
						sourceMap : true,
						sourceMapName : "www/js/main.min.js.map"
					},
	    		files: {
	    			'www/js/main.min.js': ['www/dist/main.js'],
	    		}
			}
		},
		cssmin: {
		 	options: {
				mergeIntoShorthands: false,
		   		roundingPrecision: -1
		  	},
		  	target: {
		    	files: {
		      		'www/css/main.min.css': ['www/dist/main.css']
		    	}
		  	}
		}
	});

	grunt.registerTask('default', ['concat','uglify','cssmin']);
}



