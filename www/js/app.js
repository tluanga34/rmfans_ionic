Number.prototype.getPadded = function(){

	//console.log(this);

	if(this < 10)
		return  '0' + this;
	else 
		return this.toString();

}

Array.prototype.toDhtmlSchedulerFormat = function(){
	
//{id:1, text:"Meeting",   start_date:"04/11/2013 14:00",end_date:"04/11/2013 17:00"},

	var date = null,
		ninetyLater = null,
		newArray = [];
	
	this.forEach(function(key, index){
		
		date 		= new Date(key.date);
		ninetyLater = new Date(date.getTime() + (90 * 60 * 1000));

		newArray.push({
			id 		: index + 1,
			text 	: key.homeTeamName + " vs " +key.awayTeamName,
			start_date : (date.getMonth()+1).getPadded() +"/"+date.getDate().getPadded()+"/"+date.getFullYear().getPadded() +" "+date.getHours().getPadded() +":"+date.getMinutes().getPadded(),
			end_date : (ninetyLater.getMonth()+1).getPadded() +"/"+ninetyLater.getDate().getPadded()+"/"+ninetyLater.getFullYear().getPadded() +" "+ninetyLater.getHours().getPadded() +":"+ninetyLater.getMinutes().getPadded()
		});
	});

	return newArray;

}



var app = angular.module("app",["ngRoute"]);

//CONFIGURING THE ROUTING FUNCTIONALITY
app.config(["$routeProvider",function($routeProvider){

	
	//REGISTERING ROUTES FOR ALL PAGE SECTIONS
	config.pageSections.forEach(function(key){

		$routeProvider.when("/"+key.url, {
			templateUrl : key.htmlUrl,
			controller : key.controller,
			tabLabel	: key.label
		});

	});

	//REGISTERING ROUTES FOR ALL HIDDEN ROUTES i.e USER CAN'T GO TO THESE PAGES DIRECTLY FROM NAVIGATION MENU
	config.hiddenRoutes.forEach(function(key){

		$routeProvider.when("/"+key.url, {
			templateUrl : key.htmlUrl,
			controller : key.controller,
			tabLabel	: key.label
		});

	});

	


}]);


//WE WILL KEEP ALL THE COMMONLY USED CONSTRUCTORS INSIDE THIS NAME SPACE, SO WE CAN AVOID GLOBAL NAMESPACE
app.Consts = {};


//DEFINING CONSTRUCTOR AND PROTOTYPE FUNCTIONS FOR ALL TYPES OF TOGGLING MODELS
app.Consts.Toggle = function(defaultState){
	this.state 	= defaultState || false;
	this.msg	= '';
}

app.Consts.Toggle.prototype.show = function(msg){
	if(!this.state)
		this.state = true;

	if(msg != undefined)
		this.msg = msg;
};

app.Consts.Toggle.prototype.hide = function(){
	if(this.state)
		this.state = false;

	if(this.msg != '')
		this.msg = '';
};


//DEFINING CONSTRUCTOR FOR ALL KINDS OF OBJECT WHICH HAS VALUE, LIST, AND OPTIONS
app.Consts.Data = function(){
	this.value = '';
	this.list = [];
	this.options = null;
}

app.Consts.Data.prototype.setList = function(data){
	this.list = data;
}

app.Consts.Data.prototype.setValue = function(data){
	this.value = data;
}

app.Consts.Data.prototype.setOptions = function(data){
	this.options = data;
}


//DIRECTIVES DIFINITION
app.directive("ngLoading",function(){
	return {
		template : "<table class='pop-up' ng-class=\"(ngLoading.state)?'active':'inactive'\"><tr><td class='text-center whiteFont'>Loading...<br>Please wait</td></tr></table>",
		scope : {
			ngLoading : "="
		},
		link : function(scope, $elem){

			//scope.state = true;

		}
	};
});

app.directive("ngMainheight",function(){
	return {
		link : function(scope, $elem){

			var minHeight = document.documentElement.clientHeight - 100;
			$elem.css({'min-height':minHeight + 'px'});

		}
	}
});


