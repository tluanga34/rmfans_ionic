app.controller("global",["$scope","$route", "$routeParams", "dataService",function(s,$route, $routeParams, dataService){
	
	//CONSTRUCT OBJECTS
	s.log 		= console.log;
	//s.tab		= {};
	s.sideMenu 	= new app.Consts.Toggle(false);
	s.loading 	= new app.Consts.Toggle(false);
	s.errorMsg 	= new app.Consts.Toggle(false);
	s.sections 	= new app.Consts.Data();
	s.date 		= new Date();
	s.$route 	= $route;

	//INITIATE THE PROGRAM
	s.sections.setList(config.pageSections);

	
   	s.$on("$routeChangeSuccess", function(event, current, previous){
    	s.sideMenu.active = current.tabLabel;
  	});


}]);

//CONTROLLER FOR NEXT GAMES PAGE
app.controller("NextGames",["$scope","$routeParams","dataService",function(s,$routeParams,dataService){


	var loadingAnim = s.$parent.loading;

	loadingAnim.show();

	//IF TEAAMID PARAMTER IS BEING PASSED IN THE URL, GET DATA FORM THAT TEAM
	if($routeParams.teamid != undefined){
		dataService.getData(config.api.getTeamInfo + $routeParams.teamid + '/fixtures', function(response){
			
			loadingAnim.hide();

			if(response.error != null && response.error != undefined && response.error != ''){
				s.$parent.errorMsg.show("Failed to load data due to technical error.");
			} else {

				s.fixtureData = response;
				s.fixtureData.calendarData = s.fixtureData.fixtures.toDhtmlSchedulerFormat();
			}
			
		});
	}
	//IF TEAMID PARAMETER IS NOT BEING PASSWED IN THE URL, IT WILL GET REAL MADRID TEAM DATA BY DEFAULT
	else {

		dataService.getRealMadridFixtures(function(response){
			
			loadingAnim.hide();
			
			if(response.error != null && response.error != undefined && response.error != ''){
				s.$parent.errorMsg.show("Failed to load data due to technical error.");
			} else {
				s.fixtureData = response;
				s.fixtureData.calendarData = s.fixtureData.fixtures.toDhtmlSchedulerFormat();
			}
		});

	}
	
	//THIS FUNCITON WILL GET EACH DETAILS OF HOME AND AWAY AS WELL AS THE COMPETITION INFO. THIS IS MAINLY DONE TO GET THEIR LOGO
	s.getFixtureInfo = function(x){

		if(x.homeTeamInfo == undefined){
			dataService.getData(x._links.homeTeam.href, function(response){
				x.homeTeamInfo = response;
				//console.log(x);
			});
		}

		if(x.awayTeamInfo == undefined){
			dataService.getData(x._links.awayTeam.href, function(response){
				x.awayTeamInfo = response;
				//console.log(x);
			});
		}

		if(x.competitionInfo == undefined){
			dataService.getData(x._links.competition.href, function(response){
				x.competitionInfo = response;
				//console.log(x);
			});
		}
	}

}]);




app.controller("TeamPlayers",["$scope","$routeParams","dataService",function(s,$routeParams,dataService){

	var loadingAnim = s.$parent.loading;
	
	s.teamid = $routeParams.teamid;
	
	loadingAnim.show();
	
	//IF TEAAMID PARAMTER IS BEING PASSED IN THE URL, GET DATA FORM THAT TEAM
	if(s.teamid != undefined){
		dataService.getData(config.api.getTeamInfo + s.teamid + '/players', function(response){
			
			loadingAnim.hide();

			if(response.error != null && response.error != undefined && response.error != ''){
				s.$parent.errorMsg.show("Failed to load data due to technical error.");
			} else {
				s.players = response;
			}
			
		});
	}
	//IF TEAMID PARAMETER IS NOT BEING PASSWED IN THE URL, IT WILL GET REAL MADRID TEAM DATA BY DEFAULT
	else {
		dataService.getRealMadridPlayers(function(response){
			loadingAnim.hide();

			if(response.error != null && response.error != undefined && response.error != ''){
				s.$parent.errorMsg.show("Failed to load data due to technical error.");
			} else {
				s.players = response;
			}
		});
	}
	
	
	s.getAge = function(dateString) {
		//console.log(dateString);
		var today = new Date();
		var birthDate = new Date(dateString);
		var age = today.getFullYear() - birthDate.getFullYear();
		var m = today.getMonth() - birthDate.getMonth();
		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age--;
		}
		return age;
	}
}]);


app.controller("LeagueTable",["$scope","dataService",function(s,dataService){

	var loadingAnim = s.$parent.loading;

	loadingAnim.show();

	dataService.getLaligaTable(function(response){
		
		loadingAnim.hide();
		if(response.error != null && response.error != undefined && response.error != ''){
			s.$parent.errorMsg.show("Failed to load data due to technical error.");
		} else {
			s.leagueTable = response;
		}		

	});


}]);

app.controller("LeagueFixtures",["$scope","dataService",function(s,dataService){

	var loadingAnim = s.$parent.loading;

	loadingAnim.show();

	dataService.getLaligaFixtures(function(response){
		loadingAnim.hide();
		if(response.error != null && response.error != undefined && response.error != ''){
			s.$parent.errorMsg.show("Failed to load data due to technical error.");
		} else {
			s.leagueFixtures = response;
		}		
	});

}]);

//TEAM INFO CONTROLLER. THIS PAGE WILL GET TEAM ID AS PARAMETER
app.controller("TeamInfo",["$scope","$routeParams","dataService",function(s,$routeParams,dataService){
	//console.log($routeParams);

	s.teamid = $routeParams.teamid;

	var loadingAnim = s.$parent.loading;

	loadingAnim.show();

	//IF TEAAMID PARAMTER IS BEING PASSED IN THE URL, GET DATA FORM THAT TEAM
	if(s.teamid != undefined){
		dataService.getData(config.api.getTeamInfo + $routeParams.teamid, function(response){
			
			loadingAnim.hide();

			if(response.error != null && response.error != undefined && response.error != ''){
				s.$parent.errorMsg.show("Failed to load data due to technical error.");
			} else {
				s.teamInfo = response;
			}
		});
	}
}]);
