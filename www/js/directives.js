
app.directive("ngDhtmlscheduler",["$filter",function($filter){
	return {

		template : "<div id='scheduler_here' class='dhx_cal_container' style='width:100%; height:100%;'><div class='dhx_cal_navline'><div class='dhx_cal_prev_button'>&nbsp;</div><div class='dhx_cal_next_button'>&nbsp;</div><div class='dhx_cal_today_button'></div><div class='dhx_cal_date'></div><div class='dhx_cal_tab' name='day_tab' style='right:204px;'></div><div class='dhx_cal_tab' name='week_tab' style='right:140px;'></div><div class='dhx_cal_tab' name='month_tab' style='right:76px;'></div></div><div class='dhx_cal_header'></div><div class='dhx_cal_data'></div></div>",
		scope : {
			ngDhtmlscheduler : "="
		},

		link : function(scope, $elem){

			
			scheduler.init('scheduler_here', new Date(),"month");
			var loaded = false;


			scope.$watch("ngDhtmlscheduler",function(newValue){

				console.log("Changed");
				//console.log(newValue);

				if(newValue != undefined && newValue != null && newValue != '' && !loaded){
					console.log(newValue);
					scheduler.clearAll();
					scheduler.parse(newValue, "json");
					loaded = true;
				}
					
			});

			scheduler.attachEvent("onEventSave",function(id,ev,is_new){
				console.log(ev);
				var startDate = $filter('date')(ev.start_date, 'yyyyMMddTHHmmss');
				var endDate = $filter('date')($filter('afterNinty')(ev.end_date), 'yyyyMMddTHHmmss');
			
				window.open("https://www.google.com/calendar/render?action=TEMPLATE&dates="+startDate+"/"+endDate+"&text="+ev.text+"&details="+ev.text);

				return true;
			});
		}
	}
}]);
