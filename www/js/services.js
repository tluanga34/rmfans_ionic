app.service("dataService", ["$http", function ($http) {

	var self = this,
		api = config.api;


	//Data STORED PRIVATELY
	//SET DATA TO NULL INITIALLY. THIS WILL HELP TO CHECK IF DATA IS INITIATED OR NOT
	data = {
		realMadridIndexData: null,
		realMadridFixtures: null,
		laligaTable: null,
		laligaFixtures: null,
		laligaTeams: null,
		realMadridPlayers: null,
	};

	//COMMON FUNCTION THAT CALLS HTTP REQUEST
	self.getData = function (apiUrl, callBack) {

		$http({
			url: apiUrl,
			headers: api.headers
		}).then(
			function (success) {
				var responseData = success.data;
				//console.log(responseData);
				//data.realMadridFixtures = responseData;
				callBack(responseData);
			},
			function (failed) {
				console.log("Failed due to technical Error");
				callBack({ error: "Failed due to technical Error" });
			}
			);


	}

	self.getRealMadridIndexData = function (callBack) {
		if (data.realMadridFixtures != null) {
			callBack(data.realMadridFixtures);
			return;
		}

		self.getData(api.getRmIndex, function (data) {
			data.realMadridFixtures = data;
			callBack(data);
		});
	}



	self.getRealMadridFixtures = function (callBack) {

		//RETURN RIGHT AWAY IF DATA IS REQUESTED AND STORED
		if (data.realMadridFixtures != null) {
			callBack(data.realMadridFixtures);
			return;
		}

		self.getData(api.getAllRealMadridFixtures, function (response) {
			data.realMadridFixtures = response;
			callBack(response);
		});

	}


	self.getLaligaTeams = function (callBack) {

		//RETURN RIGHT AWAY IF DATA IS REQUESTED AND ALREDY STORED
		if (data.laligaTeams != null) {
			callBack(data.laligaTeams);
			return;
		}

		self.getData(api.getAllLaligaTeams, function (response) {
			data.laligaTeams = response;
			callBack(response);
		});

	}

	self.getLaligaTable = function (callBack) {
		//RETURN RIGHT AWAY IF DATA IS REQUESTED AND ALREDY STORED
		if (data.laligaTable != null) {
			callBack(data.laligaTable);
			return;
		}

		self.getData(api.getAllLaligaTable, function (response) {
			data.laligaTable = response;
			callBack(response);
		});
	}

	self.getLaligaFixtures = function (callBack) {
		//RETURN RIGHT AWAY IF DATA IS REQUESTED AND ALREDY STORED
		if (data.laligaFixtures != null) {
			callBack(data.laligaFixtures);
			return;
		}

		self.getData(api.getAllLaligaFixtures, function (response) {
			data.laligaFixtures = response;
			callBack(response);
		});
	}

	//realMadridPlayers

	self.getRealMadridPlayers = function (callBack) {
		//RETURN RIGHT AWAY IF DATA IS REQUESTED AND ALREDY STORED
		if (data.realMadridPlayers != null) {
			callBack(data.realMadridPlayers);
			return;
		}

		self.getData(api.getAllRealMadridPlayers, function (response) {
			data.realMadridPlayers = response;
			callBack(response);
		});
	}

}]);


//CONSTRUCTOR SERVICE
app.service("dataModel", function () {

	this.Construct = function () {
		this.state = false;
		this.list = [];
		this.value = "";
	}

	this.Construct.prototype.hide = function () {
		this.state = false;
	}
	this.Construct.prototype.show = function () {
		this.state = true;
	}
	this.Construct.prototype.toggle = function () {
		this.state = !this.state;
	}

	this.Construct.prototype.setValue = function (value) {
		this.value = value;
	}

	this.Construct.prototype.clearValue = function () {
		this.value = "";
	}

	this.Construct.prototype.setValueFromListByKey = function (keyName) {

		var self = this;
		for (var i = 0; i < self.list.length; i++) {
			if (self.list[i][keyName] == true) {
				self.value = self.list[i];
				break;
			}
		}
	}

	this.Construct.prototype.setList = function (arr) {
		this.list = arr;
	}

	this.Construct.prototype.clearList = function () {
		this.list = [];
	}


	this.Construct.prototype.showMsg = function (msg) {
		this.value = msg;
		this.state = true;
	}

	this.Construct.prototype.hideMsg = function (msg) {
		this.value = "";
		this.state = false;
	}


});