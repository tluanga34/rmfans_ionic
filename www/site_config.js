

var demoApiPath = "http://localhost/rmFans/demodata/";


var config = {


	pageSections : [
		{
			label : "Home",
			url : "",
			htmlUrl : "template/next_games.html",
			controller : 'NextGames'
		},
		{
			label : "Fixtures",
			url : "fixtures",
			htmlUrl : "template/team_fixtures.html",
			controller : 'NextGames'
		},
		{
			label : "Results",
			url : "results",
			htmlUrl : "template/team_results.html",
			controller : 'NextGames'
		},
		{
			label : "Players",
			url : "players",
			htmlUrl : "template/team_players.html",
			controller : 'TeamPlayers'
		},
		{
			label : "Laliga Table",
			url : "leaguetable",
			htmlUrl : "template/league_table.html",
			controller : 'LeagueTable'
		},
		{
			label : "Laliga Fixtures",
			url : "leaguefixtures",
			htmlUrl : "template/league_fixtures.html",
			controller : 'LeagueFixtures'
		},
		{
			label : "Others Competitions",
			url : "leaguefixtures",
			htmlUrl : "template/league_fixtures.html",
			controller : 'LeagueFixtures'
		},
		
	],

	hiddenRoutes : [
		{
			label : "Team Info",
			url : "team",
			htmlUrl : "template/team_info.html",
			controller : 'TeamInfo'
		},
		{
			label : "Next Games",
			url : "nextgames",
			htmlUrl : "template/next_games.html",
			controller : 'NextGames'
		},
	],

	api : {
		headers : {
			"X-Auth-Token" : "3dadac39237a43fd95dfdfdbe792ead2",
			"Content-Type" : "application/json"
		},
		
		///*
		getRmIndex 					: "http://api.football-data.org/v1/teams/86",
		getAllFootballCompetitions 	: "http://api.football-data.org/v1/competitions",
		getAllLaligaTeams 			: "http://api.football-data.org/v1/competitions/436/teams",
		getAllLaligaTable 			: "http://api.football-data.org/v1/competitions/436/leagueTable",
		getAllLaligaFixtures 		: "http://api.football-data.org/v1/competitions/436/fixtures",
		getAllRealMadridFixtures 	: "http://api.football-data.org/v1/teams/86/fixtures",
		getAllRealMadridPlayers		: "http://api.football-data.org/v1/teams/86/players",
		getTeamInfo					: "http://api.football-data.org/v1/teams/",

		
		/*

		getRmIndex 					: demoApiPath+ "getRmIndexData.php",
		getAllLaligaTeams 			: demoApiPath+ "getTeam.php",
		getAllRealMadridFixtures 	: demoApiPath+ "getrmfixtures.php",
		getAllLaligaFixtures 		: demoApiPath+ "getLaligaFixtures.php",
		getAllLaligaTable 			: demoApiPath+ "getLaligaTable.php",
		getAllRealMadridPlayers		: demoApiPath + "getRMPlayers.php",
		getTeamInfo					: "http://api.football-data.org/v1/teams/",
		//*/
	}


};